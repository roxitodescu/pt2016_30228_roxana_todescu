package polinom;


public abstract class Polinom {
	
abstract int getGrad();
abstract int[] StringToInt();
abstract double[] StringToDouble();
}
