package polinom;

import java.text.DecimalFormat;

public class PolinomIntreg extends Polinom
{
   private String coeficienti;
   private String exponenti;
   private int[] coef = new int[100000];
   private int[] expo= new int[100000];
   private int[] poli= new int[100000];
   private int grad;
   
   //constructorul clasei ce seteaza
   // coeficientii si exponentii 
   // cu datele primite de la interfata
   public PolinomIntreg(String coeficienti,String exponenti)
   {
       this.coeficienti=coeficienti;
       this.exponenti=exponenti;
   }
   
   //transformam stringurile de coef si expo
   // in formula: polinom[exponent]=coeficient
   public int[] StringToInt()
   {
       int[] nrSpatiu = new int[20];
       int[] nrSpatiu2 = new int[20];
       int i=0;
       int n=0;
       int k=0;
       
       coeficienti=" "+coeficienti+" 0";
       for(i=0;i<coeficienti.length();i++)
       {
           try{
           if(coeficienti.charAt(i)==' ') 
           {
               nrSpatiu[n]=i;
              n++;
            }
        }catch(Exception e){}
        }    
       
        for(i=0;i<coeficienti.length();i++)
        {
       try{
          coef[i]=Integer.parseInt(coeficienti.substring(nrSpatiu[i]+1,nrSpatiu[i+1]));
          
        }catch(Exception e){};
        }   
        
        exponenti=" "+exponenti+" 0";
        for(i=0;i<exponenti.length();i++)
        {
            try{
            if(exponenti.charAt(i)==' ') 
            {
                nrSpatiu2[k]=i;
               k++;
             }
         }catch(Exception e){}
         }    
        
         for(i=0;i<exponenti.length();i++)
         {
        try{
            expo[i]=Integer.valueOf(exponenti.substring(nrSpatiu2[i]+1,nrSpatiu2[i+1]));
         }catch(Exception e){};
      
        }
         grad=expo[0];
         for ( i=0;i<grad;i++)
       {
           poli[expo[i]]=coef[i];
          }
        return poli;
   }
   
  
  
   public int getGrad()
   {
       return grad;
   }

@Override
double[] StringToDouble() {
	// TODO Auto-generated method stub
	return null;
}

private int[] rezultat = new int[100];
private double[] rezi = new double[100];
private int[] p1 = new int[100];
private int[] p2= new int[100];

private int gr=0;
private int ok=0;

//definirea formatului de afisare a tipui double 
//la transformarea in string
DecimalFormat df=new DecimalFormat("0.00");
 
//constructorul clasei 
public PolinomIntreg(int[] p1,int[] p2,int grad)
{
  this.p1=p1;
  this.p2=p2;
  this.gr=grad;
  }

 
public int[] add()
{
    int i;
    for(i=0; i<=gr; i++)
       rezultat[i]=p1[i]+p2[i];
        ok=0;
    return rezultat;
}

public int[] sub()
{
    for(int i=0; i<=gr; i++)
        rezultat[i]=p1[i]-p2[i];
    ok=0;
    return rezultat;
}

public int[] der()
{
    for(int i=1; i<=gr; i++)
        rezultat[i-1]=i*p1[i];
    ok=0;
    return rezultat;
}

public int[] multipy()
{
    for(int i=0;i<40;i++)
        for(int j=0;j<40;j++)
           {   
           rezultat[i+j]=rezultat[i+j]+p1[i]*p2[j];
           }
    ok=1;
    return rezultat;
}   public String afisare()
{
    String s="";
    if (ok==1) 
    {	gr=2*gr;
    for(int i=gr;i>0;i--)
    {
        try{
        if (rezultat[i] > 0)    
        {  s=s+ "+ "+Integer.toString(rezultat[i])+"x^"+Integer.toString(i) ;}
        else 
        { if (rezultat[i] < 0)
            { s=s+Integer.toString(rezultat[i])+"x^"+Integer.toString(i)+" ";}
        }
    }catch(Exception ev){};
    }
    if (rezultat[0] > 0)    
        {  s=s+ "+ "+Integer.toString(rezultat[0])+"x^"+Integer.toString(0) ;}
        else 
        { if (rezultat[0] < 0)
            { s=s+Integer.toString(rezultat[0])+"x^"+Integer.toString(0)+" ";}
        }
   }
   
    else if(ok==0){
    	  for(int i=gr;i>0;i--)
          {
              try{
              if (rezultat[i] > 0)    
              {  s=s+ "+ "+Integer.toString(rezultat[i])+"x^"+Integer.toString(i) ;}
              else 
              { if (rezultat[i] < 0)
                  { s=s+Integer.toString(rezultat[i])+"x^"+Integer.toString(i)+" ";}
              }
          }catch(Exception ev){};
          }
          if (rezultat[0] > 0)    
              {  s=s+ "+ "+Integer.toString(rezultat[0])+"x^"+Integer.toString(0) ;}
              else 
              { if (rezultat[0] < 0)
                  { s=s+Integer.toString(rezultat[0])+"x^"+Integer.toString(0)+" ";}
              }
        
    } 
    
    if (ok==2){
    	
    	  for(int i=gr+1;i>0;i--)
          {
              try{
              if (rezi[i] > 0)    
              {  s=s+ "+ "+df.format(rezi[i])+"x^"+Integer.toString(i) ;}
              else 
              { if (rezi[i] < 0)
                  { s=s+df.format(rezi[i])+"x^"+Integer.toString(i)+" ";}
              }
          }catch(Exception ev){};
          }
          if (rezi[0] > 0)    
              {  s=s+ "+ "+df.format(rezi[0])+"x^"+Integer.toString(0) ;}
              else 
              { if (rezi[0] < 0)
                  { s=s+df.format(rezi[0])+"x^"+Integer.toString(0)+" ";}
              }
                     
    }
  return s;
}
}
