package polinom;

import java.text.DecimalFormat;

public class PolinomReal extends Polinom {
	
		   private String coeficienti;
		   private String exponenti;
		   private int[] coef = new int[100];
		   private int[] expo= new int[100];
		   private double[] poli= new double[100];
		   private int grad;

public PolinomReal(String coeficienti,String exponenti)
{
    this.coeficienti=coeficienti;
    this.exponenti=exponenti;
}
public double[] StringToDouble()
{
    int[] nrSpatiu = new int[20];
    int[] nrSpatiu2 = new int[20];
    int i=0;
    int n=0;
    int k=0;
    
    coeficienti=" "+coeficienti+" 0";
    for(i=0;i<coeficienti.length();i++)
    {
        try{
        if(coeficienti.charAt(i)==' ') 
        {
            nrSpatiu[n]=i;
           n++;
         }
     }catch(Exception e){}
     }    
    
     for(i=0;i<coeficienti.length();i++)
     {
    try{
       coef[i]=Integer.parseInt(coeficienti.substring(nrSpatiu[i]+1,nrSpatiu[i+1]));
       
     }catch(Exception e){};
     }   
     
     exponenti=" "+exponenti+" 0";
     for(i=0;i<exponenti.length();i++)
     {
         try{
         if(exponenti.charAt(i)==' ') 
         {
             nrSpatiu2[k]=i;
            k++;
          }
      }catch(Exception e){}
      }    
     
      for(i=0;i<exponenti.length();i++)
      {
     try{
         expo[i]=Integer.valueOf(exponenti.substring(nrSpatiu2[i]+1,nrSpatiu2[i+1]));
      }catch(Exception e){};
   
     }
      grad= expo[0];
      for ( i=0;i<grad;i++)
    {
        poli[(int) expo[i]]=coef[i];
       }
     return poli;
}



public int getGrad()
{
    return (int) grad;
}
@Override
int[] StringToInt() {
	// TODO Auto-generated method stub
	return null;
}
private double[] rezultat = new double[10000000];
private double[] rezi = new double[10000000];
private int[] p1 = new int[10000];
private int[] p2= new int[100000];

private int gr=0;
private int ok=0;

//definirea formatului de afisare a tipui double 
//la transformarea in string
DecimalFormat df=new DecimalFormat("0.00");
 
//constructorul clasei 
public PolinomReal(int[] p1,int[] p2,int grad)
{
  this.p1=p1;
  this.p2=p2;
  this.gr=grad;
  } 
public double[] impartire(){
	int i,grad1=0,grad2=0;
	for(i=0;i<100;i++){
		if(p1[i] !=0)
			grad1=i;
		if(p2[i] !=0)
			grad2=i;
	}
	while(grad1>=grad2){
		 rezultat[grad1-grad2]=p1[grad1]/p2[grad2];
		for(i=0;i<grad2;i++){
			p1[i+grad1-grad2]=(int) (p1[i+grad1-grad2]-rezultat[grad1-grad2]*p2[i]);
		}
		grad1--;
	}
	ok=0;
  	return rezultat;
}

public double[] integrare()
{
    for(int i=0;i<=gr+1;i++)
        rezi[i+1]=(double)(p1[i])/(i+1);
    ok=2;
    return rezi;
}
public String afisare()
{
    String s="";
    if (ok==1) 
    {	gr=2*gr;
    for( int i=gr;i>0;i--)
    {
        try{
        if (rezultat[i] > 0)    
        {  s=s+ "+ "+Integer.toString((int) rezultat[i])+"x^"+Integer.toString(i) ;}
        else 
        { if (rezultat[i] < 0)
            { s=s+Integer.toString((int) rezultat[i])+"x^"+Integer.toString(i)+" ";}
        }
    }catch(Exception ev){};
    }
    if (rezultat[0] > 0)    
        {  s=s+ "+ "+Integer.toString((int) rezultat[0])+"x^"+Integer.toString(0) ;}
        else 
        { if (rezultat[0] < 0)
            { s=s+Integer.toString((int) rezultat[0])+"x^"+Integer.toString(0)+" ";}
        }
   }
   
    else if(ok==0){
    	  for(int i=gr;i>0;i--)
          {
              try{
              if (rezultat[i] > 0)    
              {  s=s+ "+ "+Integer.toString((int) rezultat[i])+"x^"+Integer.toString(i) ;}
              else 
              { if (rezultat[i] < 0)
                  { s=s+Integer.toString((int) rezultat[i])+"x^"+Integer.toString(i)+" ";}
              }
          }catch(Exception ev){};
          }
          if (rezultat[0] > 0)    
              {  s=s+ "+ "+Integer.toString((int) rezultat[0])+"x^"+Integer.toString(0) ;}
              else 
              { if (rezultat[0] < 0)
                  { s=s+Integer.toString((int) rezultat[0])+"x^"+Integer.toString(0)+" ";}
              }
        
    } 
    
    if (ok==2){
    	
    	  for(int i=gr+1;i>0;i--)
          {
              try{
              if (rezi[i] > 0)    
              {  s=s+ "+ "+df.format(rezi[i])+"x^"+Integer.toString(i) ;}
              else 
              { if (rezi[i] < 0)
                  { s=s+df.format(rezi[i])+"x^"+Integer.toString(i)+" ";}
              }
          }catch(Exception ev){};
          }
          if (rezi[0] > 0)    
              {  s=s+ "+ "+df.format(rezi[0])+"x^"+Integer.toString(0) ;}
              else 
              { if (rezi[0] < 0)
                  { s=s+df.format(rezi[0])+"x^"+Integer.toString(0)+" ";}
              }
                     
    }
  return s;
}
}