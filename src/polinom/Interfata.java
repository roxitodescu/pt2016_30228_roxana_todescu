package polinom;

//import java.util.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
//import javax.swing.event.*;

//import java.awt.Graphics;


public class Interfata
{
  private int[] poli1= new int[100];
  private int[] poli2= new int[100];
//  private int[] rez= new int[100];
//  private double[] rezul=new double[100];
  private int grad;
  
  JLabel ex1=new JLabel("Exponentii polinomului 1:");
  JLabel ex2=new JLabel("Exponentii polinomului 2:");
  JLabel coef1=new JLabel("Coeficientii polinomului 1:");
  JLabel coef2=new JLabel("Coeficientii polinomului 2:");
  JLabel afis=new JLabel("Afisare rezultate:");
  
  JButton plus=new JButton("+");
  JButton minus=new JButton("-");
  JButton divi=new JButton("/");
  JButton ori=new JButton("*");
  JButton deri=new JButton("'");
  JButton integ=new JButton("~");
  
  String cPol1; // coeficientii primului polinom
  String cPol2; // coeficientii celui de-al 2 polinom
  String ePol1; // exponenti 1
  String ePol2; // exponenti 2
  JTextField t1=new JTextField(20);
  JTextField t2=new JTextField(20);
  JTextField t3=new JTextField(20);
  JTextField t4=new JTextField(20);
  JTextField t5=new JTextField(20);
   Color c= new Color(0,191,255);
  /**
   * constructorul clasei:
   */
  public Interfata()
   {
    
    JFrame f=new JFrame();
    f.setTitle("Procesarea Polinoamelor");
    f.setVisible(true);
    f.setSize(1400, 736);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JPanel p= new JPanel(new GridBagLayout());
    f.getContentPane().add(p,BorderLayout.CENTER);
    p.setBackground(new Color(0,191,255));
    
    GridBagConstraints con= new GridBagConstraints();
    con.insets= new Insets(10,10,10,10);
    
    
  // aranjarea in pagina a componentelor interfetei
  con.gridx=0;
  con.gridy=0;
  p.add(ex1,con);
  con.gridx=1;
  con.gridy=0;
  p.add(t1,con);
  
  con.gridx=8;
  con.gridy=0;
  p.add(ex2,con);
  con.gridx=9;
  con.gridy=0;
  p.add(t2,con);
  
  con.gridx=0;
  con.gridy=1;
  p.add(coef1,con);
  con.gridx=1;
  con.gridy=1;
  p.add(t3,con);
  
  con.gridx=8;
  con.gridy=1;
  p.add(coef2,con);
  con.gridx=9;
  con.gridy=1;
  p.add(t4,con);
  
  con.gridx=2;
  con.gridy=3;
  p.add(plus,con);
  con.gridx=3;
  con.gridy=3;
  p.add(minus,con);
  con.gridx=4;
  con.gridy=3;
  p.add(divi,con);
  con.gridx=5;
  con.gridy=3;
  p.add(ori,con);
  con.gridx=6;
  con.gridy=3;
  p.add(deri,con);
  con.gridx=7;
  con.gridy=3;
  p.add(integ,con);
  
  con.gridx=5;
  con.gridy=4;
  p.add(afis,con);
  
  con.gridx=5;
  con.gridy=5;
  p.add(t5,con);
  
  //sa nu se poata modifica textul de la rezultat
  t5.setEditable(false);
  
  // listeneri pentru butoanele de la operatii
  plus.addActionListener(new ActionListener() 
  {
      
      public void actionPerformed(ActionEvent e) {
          ePol1=new String(t3.getText());// salveaza exponentii polinomului 1
          ePol2=new String(t4.getText());//salveaza exponentii polinomului 2
          
          cPol1=new String(t1.getText());//coeficientii polinomului 1
          cPol2=new String(t2.getText());//coeficientii polinomului 2
          
          Polinom p1 = new PolinomIntreg(ePol1,cPol1);
          Polinom p2 = new PolinomIntreg(ePol2,cPol2);
           poli1=p1.StringToInt();
           poli2=p2.StringToInt();
           if(p1.getGrad()>p2.getGrad())
              { grad=p1.getGrad();}
              else 
              {grad=p2.getGrad();}
            
           PolinomIntreg op = new PolinomIntreg(poli1,poli2,grad);
         op.add();
          t5.setText(op.afisare());
      }
  });
  
  minus.addActionListener(new ActionListener() 
  {
      
      public void actionPerformed(ActionEvent e) {
          ePol1=new String(t3.getText());
          ePol2=new String(t4.getText());
          
          cPol1=new String(t1.getText());
          cPol2=new String(t2.getText());
          
          Polinom p1 = new PolinomIntreg(ePol1,cPol1);
          Polinom p2 = new PolinomIntreg(ePol2,cPol2);
           poli1=p1.StringToInt();
           poli2=p2.StringToInt();
           if(p1.getGrad()>p2.getGrad())
              { grad=p1.getGrad();}
              else 
              {grad=p2.getGrad();}
            
           PolinomIntreg op = new PolinomIntreg(poli1,poli2,grad);
          op.sub();
          t5.setText(op.afisare());
      }
  });
  divi.addActionListener(new ActionListener() 
  {
      
      public void actionPerformed(ActionEvent e) {
      	  ePol1=new String(t3.getText());
            ePol2=new String(t4.getText());
            
            cPol1=new String(t1.getText());
            cPol2=new String(t2.getText());
            
            Polinom p1 = new PolinomIntreg(ePol1,cPol1);
            Polinom p2 = new PolinomIntreg(ePol2,cPol2);
             poli1=p1.StringToInt();
             poli2=p2.StringToInt();
             if(p1.getGrad()>p2.getGrad())
                { grad=p1.getGrad();}
                else 
                {grad=p2.getGrad();}
              
             PolinomReal op = new PolinomReal(poli1,poli2,grad);
             op.impartire();
            t5.setText(op.afisare());
        }
    });
  
  
  ori.addActionListener(new ActionListener() 
  {
      
      public void actionPerformed(ActionEvent e) {
          ///try{
          ePol1=new String(t3.getText());
          ePol2=new String(t4.getText());
          
          cPol1=new String(t1.getText());
          cPol2=new String(t2.getText());
          
          Polinom p1 = new PolinomIntreg(ePol1,cPol1);
          Polinom p2 = new PolinomIntreg(ePol2,cPol2);
           poli1=p1.StringToInt();
           poli2=p2.StringToInt();
           if(p1.getGrad()>p2.getGrad())
              { grad=p1.getGrad();}
              else 
              {grad=p2.getGrad();}
            
           PolinomIntreg op = new PolinomIntreg(poli1,poli2,grad);
           op.multipy();
          t5.setText(op.afisare());
      }
  });
  
  
  deri.addActionListener(new ActionListener() 
  {
      
      public void actionPerformed(ActionEvent e) {
      	 ///try{
          ePol1=new String(t3.getText());
          ePol2=new String(t4.getText());
          
          cPol1=new String(t1.getText());
          cPol2=new String(t2.getText());
          
          Polinom p1 = new PolinomIntreg(ePol1,cPol1);
          Polinom p2 = new PolinomIntreg(ePol2,cPol2);
           poli1=p1.StringToInt();
           poli2=p2.StringToInt();
           if(p1.getGrad()>p2.getGrad())
              { grad=p1.getGrad();}
              else 
              {grad=p2.getGrad();}
            
           PolinomIntreg op = new PolinomIntreg(poli1,poli2,grad);
           op.der();
          t5.setText(op.afisare());
      }
  });
  
  
  integ.addActionListener(new ActionListener() 
  {
      
      public void actionPerformed(ActionEvent e) {
      	 ///try{
          ePol1=new String(t3.getText());
          ePol2=new String(t4.getText());
          
          cPol1=new String(t1.getText());
          cPol2=new String(t2.getText());
          
          Polinom p1 = new PolinomIntreg(ePol1,cPol1);
          Polinom p2 = new PolinomIntreg(ePol2,cPol2);
           poli1=p1.StringToInt();
           poli2=p2.StringToInt();
           if(p1.getGrad()>p2.getGrad())
              { grad=p1.getGrad();}
              else 
              {grad=p2.getGrad();}
            
           PolinomReal op = new PolinomReal(poli1,poli2,grad);
           op.integrare();
          t5.setText(op.afisare());
      }
  });
  
  
  
  
   }

}
